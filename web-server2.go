package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"text/template"
)

// http://localhost:7676
var PORT = ":7676"

type Employee struct {
	ID       int
	Name     string
	Age      int
	Division string
}

var employees = []Employee{
	{ID: 1, Name: "Bobi", Age: 23, Division: "IT"},
	{ID: 2, Name: "Andi", Age: 24, Division: "HR"},
	{ID: 3, Name: "Tomi", Age: 23, Division: "IT"},
}

func main() {
	//HandleFunc for routing/controller
	// http://localhost:7676/employees
	//Get
	http.HandleFunc("/employees", getEmployees)

	//POST
	http.HandleFunc("/employee", createEmployee)

	fmt.Println("Applications is running on port", PORT)
	http.ListenAndServe(PORT, nil)
}

// GET
func getEmployees(w http.ResponseWriter, r *http.Request) {
	// w.Header().Set("Content-Type", "application/json")

	if r.Method == "GET" {
		tpl, err := template.ParseFiles("index.html")

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		tpl.Execute(w, employees)		
		return		
	}

	http.Error(w, "Invalid method", http.StatusBadRequest)
}

// POST
func createEmployee(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	if r.Method == "POST" {
		name := r.FormValue("name")
		age := r.FormValue("age")
		division := r.FormValue("division")

		convertAge, err := strconv.Atoi(age)

		if err != nil {
			http.Error(w, "Invalid age", http.StatusBadRequest)
			return
		}

		newEmployee := Employee{
			ID:       len(employees) + 1,
			Name:     name,
			Age:      convertAge,
			Division: division,
		}

		employees = append(employees, newEmployee)

		json.NewEncoder(w).Encode(newEmployee)
		return
	}
	http.Error(w, "Invalid method", http.StatusBadRequest)
}
